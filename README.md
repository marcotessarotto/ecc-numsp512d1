This code defines a numsp512d1 eliptic curve and generates a public and private key pair from it using the openssl library.
To compile with gcc you need to use the "-lcrpyto" flag to link the library.

Please note that this was an exercise for an university class, so i do not raccomend using it for any "real" application.

